const bcrypt = require("bcrypt");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const userModel = require("../dao/models/user");

const saltRounds = 10;

//logica de passport en login
const loginVerifyCallback = (req, username, password, done) => {
  userModel
    .findOne({ username })
    .then(async (user) => {
      if (!user) return done(null, false);
      const { hash } = user;
      //bcrypt para comparar passwords
      return (await bcrypt.compare(password, hash))
        ? done(null, user)
        : done(null, false);
    })
    .catch((err) => {
      done(err);
    });
};

//logica de passport en el register
const registerVerifyCallback = (req, username, password, done) => {
  userModel
    .findOne({ username })
    .then(async (user) => {
      if (user) return done(null, false);
      else {
        //bcrypt para hashear password
        const hash = await bcrypt.hash(password, saltRounds);
        const newUser = { username, hash };
        userModel
          .create(newUser)
          .then((user) => {
            return done(null, user);
          })
          .catch((err) => {
            done(err);
          });
      }
    })
    .catch((err) => done(err));
};

const registerStrategy = new LocalStrategy(
  { passReqToCallback: true },
  registerVerifyCallback
);

const loginStrategy = new LocalStrategy(
  { passReqToCallback: true },
  loginVerifyCallback
);

passport.use("register-local", registerStrategy);
passport.use("login-local", loginStrategy);

passport.serializeUser((user, done) => {
  return done(null, user.id);
});

passport.deserializeUser((id, done) => {
  userModel
    .findById(id)
    .then((user) => {
      return done(null, user);
    })
    .catch((err) => done(err));
});
