require("dotenv").config();
const PORT_FROM_ARGS = process.argv[2];
const FB_CLIENT_ID_FROM_ARGS = process.argv[3];
const FB_CLIENT_SECRET_FROM_ARGS = process.argv[4];

module.exports = {
  PORT: PORT_FROM_ARGS || process.env.PORT || 8080,
  MONGO_URI: process.env.MONGO_URI || "undefined",
  SECRET_KEY: process.env.SECRET_KEY || "qwertyuiop",
  FB_CLIENT_ID: FB_CLIENT_ID_FROM_ARGS || process.env.FB_CLIENT_ID || "id",
  FB_CLIENT_SECRET:
    FB_CLIENT_SECRET_FROM_ARGS || process.env.FB_CLIENT_SECRET || "secret",
};
