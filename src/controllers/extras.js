const info = require("../utils/process-info");
const { fork } = require("child_process");

process.on("exit", (exit) => {
  console.log(`server exited with code ${exit}`);
});

exports.forceShutdown = async (req, res, next) => {
  process.exit();
};

exports.info = async (req, res, next) => {
  res.json(info);
};

exports.randoms = async (req, res, next) => {
  const forked = fork("src/utils/child.js");

  cant = req.query.cant;

  forked.send(cant || null);
  forked.on("message", (cant) => {
    res.json(cant);
  });
};
