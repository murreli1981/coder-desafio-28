const { Schema, model } = require("mongoose");

const productSchema = new Schema({
  title: String,
  price: String,
  thumbnail: String,
});
// Objeto o la clase que me da acceso a los métodos para hacer el crud
module.exports = model("Product", productSchema);
