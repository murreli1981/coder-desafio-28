const getRandoms = (iterations) => {
  const _frequency = {};
  for (i = 0; i < iterations; i++) {
    const random = Math.floor(Math.random() * (1000 + 1));
    _frequency[random] = _frequency[random] + 1 || 1;
  }
  return _frequency;
};

process.on("message", (qt) => {
  const _default = 100000000;
  const _iterations = qt || _default;
  console.log(`...generating ${_iterations} random numbers, please wait`);
  const randoms = getRandoms(_iterations);
  process.send(randoms);
});
