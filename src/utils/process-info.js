const info = {
  "Argumentos de entrada": process.argv.slice(2),
  "Sistema Operativo": process.platform,
  "Version de NodeJS": process.version,
  "Uso de memoria": process.memoryUsage(),
  "Path de ejecución": process.execPath,
  "Process Id": process.pid,
  "Carpeta corriente": process.cwd(),
};

module.exports = info;
